FROM composer:latest
# install all PHP dependencies
COPY . /app
RUN composer install


FROM php:7.2-apache
COPY --from=0 /app /var/www/html/
RUN apt-get update
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y vim
RUN docker-php-ext-install zip
RUN docker-php-ext-install pdo pdo_mysql
RUN docker-php-ext-enable pdo pdo_mysql
RUN a2enmod rewrite
COPY ./000-default.conf /etc/apache2/sites-available/
EXPOSE 80